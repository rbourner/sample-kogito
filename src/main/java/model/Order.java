package model;

public class Order {

    private int price;
    private boolean approved;

    public int getPrice() {
	return this.price;
    }

   public void setPrice(int p) {
	this.price = p;
   }

   public boolean isApproved() {
	return this.approved;
   }

   public void setApproved(boolean app) {
	this.approved = app;
   }

}

